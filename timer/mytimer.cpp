#include "mytimer.h"
#include <QtCore>
#include <QDebug>

MyTimer::MyTimer()
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(MySlot()));

    timer->start(2000);
}

MyTimer::~MyTimer()
{

}

void MyTimer::MySlot()
{
    qDebug() << "Timer executed";
}
