#-------------------------------------------------
#
# Project created by QtCreator 2015-02-23T15:44:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = template
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mydialog.cpp

HEADERS  += mainwindow.h \
    mydialog.h

FORMS    += mainwindow.ui \
    mydialog.ui
