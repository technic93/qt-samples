#-------------------------------------------------
#
# Project created by QtCreator 2015-02-23T19:16:33
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = res-files
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

RESOURCES += \
    myresources.qrc
