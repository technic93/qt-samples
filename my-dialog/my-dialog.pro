#-------------------------------------------------
#
# Project created by QtCreator 2015-02-23T17:13:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = my-dialog
TEMPLATE = app


SOURCES += main.cpp\
        mydialog.cpp

HEADERS  += mydialog.h

FORMS    += mydialog.ui
