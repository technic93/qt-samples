#include <QCoreApplication>
#include <QDebug>
#include <QStringList>
#include <QVector>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList stList;
    stList << "a" << "b" << "c" << "d" << "e";

    QVector<QString> vct(5);

    //Copies the items from range [begin1, end1) to range [begin2, ...),
    //in the order in which they appear.
    qCopy(stList.begin() + 1, stList.end(), vct.begin());

    foreach (QString item, vct) {
        qDebug() << item;
    }

    return a.exec();
}
