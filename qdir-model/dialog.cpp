#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    dirModel = new QDirModel(this);
    dirModel->setReadOnly(false);
    dirModel->setSorting(QDir::DirsFirst | QDir::IgnoreCase | QDir::Name);

    ui->treeView->setModel(dirModel);

    QModelIndex index = dirModel->index("E:/");
    ui->treeView->expand(index);
    ui->treeView->scrollTo(index);
    ui->treeView->setCurrentIndex(index);
    ui->treeView->resizeColumnToContents(0);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    //make dir
    QModelIndex index = ui->treeView->currentIndex();
    if (!index.isValid()) return;

    QString name = QInputDialog::getText(this, "Name", "Enter a name");
    if (name.isEmpty()) return;
    dirModel->mkdir(index, name);
}

void Dialog::on_pushButton_2_clicked()
{
    //delete
    QModelIndex index = ui->treeView->currentIndex();
    if (!index.isValid()) return;

    if (dirModel->fileInfo(index).isDir()) {
        //dir
        dirModel->rmdir(index);
    } else {
        //file
        dirModel->remove(index);
    }
}
