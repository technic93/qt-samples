#include <QCoreApplication>
#include <QMap>
#include <QHash>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // You can just change QMap/QMapIterator by QHash/QHashIterator
    // The differences are (from documentation):
    // 1. QHash provides faster lookups than QMap. (See Algorithmic Complexity for details.)
    // 2. When iterating over a QMap, the items are always sorted by key.
    // With QHash, the items are arbitrarily ordered.
    QMap<int, QString> employees;
    employees.insert(1, "Bob");
    employees.insert(2, "Cid");
    employees.insert(3, "Paul");
    employees.insert(4, "Mary");
    employees.insert(5, "John");

    foreach (int i, employees.keys()) {
        qDebug() << employees[i];
    }

    //Iterator
    QMapIterator<int, QString> mapIter(employees);
    while (mapIter.hasNext())
    {
        mapIter.next();
        qDebug() << mapIter.key() << " " << mapIter.value();
    }

    return a.exec();
}
