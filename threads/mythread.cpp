#include "mythread.h"
#include <QtCore>
#include <QDebug>

MyThread::MyThread()
{

}

MyThread::~MyThread()
{

}

void MyThread::run()
{
    qDebug() << this->name +  " running";

    for (int i = 0; i < 1000; ++i) {
        QMutex mutex;
        mutex.lock();

        qDebug() << this->name << i;
        if (this->Stop) break;

        mutex.unlock();
    }
}

