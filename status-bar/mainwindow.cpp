#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
#include <QtCore>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    statusLabel = new QLabel(this);
    statusLabel->setText("Downloading");

    statusProgressBar = new QProgressBar(this);
    statusProgressBar->setTextVisible(false);

    ui->statusBar->addPermanentWidget(statusLabel);
    ui->statusBar->addPermanentWidget(statusProgressBar);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionDo_something_triggered()
{
    ui->statusBar->showMessage("Hello status bar");
    statusProgressBar->setValue(49);
}
