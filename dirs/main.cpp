#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QString>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QDir mDir("E:/test/");

    foreach (QFileInfo info, mDir.entryInfoList()) {
        if (info.isDir()) qDebug() << "Dir:" << info.absoluteFilePath();
        if (info.isFile()) qDebug() << "File:" << info.absoluteFilePath();

    }

    return a.exec();
}
